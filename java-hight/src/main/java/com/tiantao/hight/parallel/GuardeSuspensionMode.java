package com.tiantao.hight.parallel;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 保护暂停模式
 * 	当请求太多时，可以使用队列将这些请求保存起来，然后通过先进先出的方式一个一个的处理请求
 * @author tiantao
 *
 */
public class GuardeSuspensionMode {
	public static void main(String[] args) {
		//1. 创建队列
		RequestQueue requestQueue = new RequestQueue();
		//2. 使用客户端模拟发送请求
		new ClientThread(requestQueue, "thread1").start();
		new ClientThread(requestQueue, "thread2").start();
		new ClientThread(requestQueue, "thread3").start();
		//3. 使用服务端，模拟接收请求（接收请求时使用队列，）
		System.out.println("处理请求：");		//这里必须打印一行数据，否则请求不会被处理
		new Serverthread(requestQueue, "thread4").start();
		new Serverthread(requestQueue, "thread5").start();
	}
}

//客户端的请求
class Request{
	private String name;
	public Request(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		return "Request [name=" + name + "]";
	}
}
//请求队列
class RequestQueue{
	//爆保存请求的队列
	private Queue<Request> queue = new ConcurrentLinkedQueue<Request>();
	
	/**
	 * 获取请求队列
	 * @return
	 */
	public synchronized Request getRequest(){
		while(queue.size()<=0){
			try { wait();
			} catch (InterruptedException e) { e.printStackTrace(); }
		}
		return queue.poll();
	}
	/**
	 * 新增一个请求
	 * @param request
	 */
	public synchronized void putRequest(Request request){
		queue.add(request);
	}
}
//客户端的进程
class ClientThread extends Thread{
	private RequestQueue requestQueue;
	public ClientThread(RequestQueue requestQueue,String name) {
		super(name);
		this.requestQueue = requestQueue;
	}
	@Override
	public void run() {
		for (int i = 0; i < 10000; i++) {
			Request request = new Request("No."+i);
			System.out.println(Thread.currentThread().getName() + ",request:"+request);
			requestQueue.putRequest(request);
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {e.printStackTrace();}
		}
	}
}


//服务端进程
class Serverthread extends Thread{
	private RequestQueue requestQueue;
	public Serverthread(RequestQueue requestQueue,String name) {
		super(name);
		this.requestQueue = requestQueue;
	}
	
	@Override
	public void run() {
		for (int i = 0; i < 10000; i++) {
			Request request = requestQueue.getRequest();
			System.out.println(Thread.currentThread().getName() + ",handles:" + request);
			
			try {
				Thread.sleep(1000L);
			} catch (InterruptedException e) {e.printStackTrace();}
			
		}
	}
}



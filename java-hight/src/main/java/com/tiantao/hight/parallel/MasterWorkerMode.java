package com.tiantao.hight.parallel;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Master-worker模式
 *  理解：
 * 	  （master和worker都是进程）
 * 	  master接收并分配任务，worker处理子任务，worker将任务处理完成之后，将结果返回master，
 * 	    由master进行归纳和汇总。
 * @author tiantao
 *
 */
public class MasterWorkerMode {
	public static void main(String[] args) throws InterruptedException {
		//1. 创建master
		Master master = new Master();
		
		//2. 添加worker到master中进行管理
		master.addWorker("worker1");
		master.addWorker("worker2");
		master.addWorker("worker3");
		
		//3. 添加任务到master中
		master.submitTask("1");
		master.submitTask("2");
		master.submitTask("3");
		master.submitTask("4");
		master.submitTask("5");
		master.submitTask("6");
		master.submitTask("7");
		master.submitTask("8");
		master.submitTask("9");
		
		//4. 执行master中所有的worker
		master.execute();
		
		//5. 查看返回结果
		Thread.sleep(3000L);
		System.out.println(master.getResultMap());
		
		
	}
}
/**
 * worker
 */
class Worker implements Runnable{
	//存储master信息
	private Master master;
	//构造方法，设置任务队列
	public Worker(Master master) {
		this.master = master;
	}
	/**
	 * worker进行要运行的内容
	 */
	public void run() {
		while (true) {
			//请求master分配下一个任务
			Object input = master.allocatingTask();
			if(input == null) {	//如果没有下一个任务，那么运行结束
				break;
			}
			
			//处理子任务
			String result = input + "bbb";
			//设置返回值
			master.collectResult(Thread.currentThread().getName()+"_"+input, result);
		}
	}
}
/**
 * master
 */
class Master {
	//任务队列
	private Queue<Object> workQueue = new ConcurrentLinkedQueue<Object>();
	//worker进程队列
	private Map<String,Thread> threadMap = new HashMap<String, Thread>();
	//结果集
	private Map<String,Object> resultMap = new HashMap<String, Object>();
	
	/**
	 * 给task分配任务
	 * @return
	 */
	public Object allocatingTask(){
		return workQueue.poll();
	}
	/**
	 * 收集执行结果
	 * @param name
	 * @param result
	 */
	public void collectResult(String name,Object result){
		resultMap.put(name, result);
	}
	/**
	 * 判断所有的任务十分都已经完成
	 * @return
	 */
	public boolean isComplete(){
		if(workQueue.size() != 0){		//如果任务队列中有任务,那么还没有完成
			return false;
		}else{							//如果队列中没有任务
			for (Thread thread : threadMap.values()) {
				if(thread.getState() != Thread.State.TERMINATED){	//如果线程中没有运行结束的，那么还没有完成
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * 提交任务
	 * @param obj
	 */
	public void submitTask(Object obj){
		workQueue.offer(obj);
	}
	
	/**
	 * 新增一个worker进程
	 * @param name
	 */
	public void addWorker(String name){
		//1. 创建worker
		Worker worker = new Worker(this);
		//2. 创建一个worker线程
		threadMap.put(name, new Thread(worker, name));
	}
	/**
	 * 运行所有worker进程
	 */
	public void execute(){
		for (Thread thread : threadMap.values()) {
			thread.start();
		}
	}
	/**
	 * 返回任务结果集
	 * @return
	 */
	public Map<String,Object> getResultMap(){
		return resultMap;
	}
}



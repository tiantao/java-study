package com.tiantao.hight.optimize.other;

import java.util.ArrayList;
import java.util.List;

/**
 * 使用clone代替new
 * 		本类实现的是浅度克隆（该对象下的引用属性，只会克隆引用属性的下标）
 * 		（也可以实现深度克隆，具体方法取网上搜！）
 * @author tiantao
 *
 */
public class CloneReplaceNew {
	
	public static void main(String[] args) throws CloneNotSupportedException {
		Student stu1 = new Student();
		stu1.setName("小明");
		stu1.setStore(98);
		stu1.setList(new ArrayList<String>());
		stu1.getList().add("java");
		
		//克隆stu1对象，并放到stu2中
		Student stu2 = stu1.cloneStu();
		stu2.setName("小红");
		stu2.getList().add("C++");
		
		System.out.println("stu1:"+stu1);	//stu1:Student [name=小明, store=98, list=[java, C++]]
		System.out.println("stu2:"+stu2);	//stu2:Student [name=小红, store=98, list=[java, C++]]
	}
}

/**
 * 并实现克隆接口
 * 	  注意：
 * 		如果该对象的属性是基本数据类型，那么会克隆该属性的值
 * 		如果该对象的属性是引用类型，那么会克隆该属性的指针
 * @author tiantao
 */
class Student implements Cloneable{
	private String name;
	private Integer store;
	private List<String> list;		//注意：该对象克隆的是指针，不是内容
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStore() {
		return store;
	}
	public void setStore(Integer store) {
		this.store = store;
	}
	public List<String> getList() {
		return list;
	}
	public void setList(List<String> list) {
		this.list = list;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", store=" + store + ", list=" + list + "]";
	}
	
	protected Student cloneStu(){
		try {
			return (Student)super.clone();	//调用clone方法，克隆本对象
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
}









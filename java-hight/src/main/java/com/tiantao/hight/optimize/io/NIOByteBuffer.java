package com.tiantao.hight.optimize.io;

import java.nio.ByteBuffer;

/**
 * 关于nio中ByteBuffer的api介绍
 * @author tiantao
 *
 */
public class NIOByteBuffer {
	
	public static void main(String[] args) {
		/**
		 * 1. 介绍如下api
		 * 	1.1 创建ByteBuffer对象，并设置缓冲区大小：ByteBuffer.allocate(15)
		 * 	1.2 添加数据api：buffer.put((byte)5);
		 * 	1.3 对缓冲区的下标做归零操作：buffer.flip();
		 * 	1.4 使用while循环读取数据：buffer.hasRemaining()，buffer.get()
		 * 	1.5 清空数据:buffer.clear();
		 */
		common();
		System.out.println("----------------------------------------\n");
		
		//2. 设置标志位，并从标志位开始读取数据
		reRead();
		System.out.println("----------------------------------------\n");
		
		//3. 拷贝缓冲区（不仅仅拷贝数据，而且将读取的状态。。。所有的都拷贝了）
		copyBuffer();
		System.out.println("----------------------------------------\n");
		
		//4. 将下标做清零操作
		indexZeroBuffer();
		System.out.println("----------------------------------------\n");
		
		//5. 创建子缓冲区，并将子缓冲区的数据与父级缓冲区的数据进行共享
		subBuffer();
		System.out.println("----------------------------------------\n");
		
		//6. 只读缓冲区（只读缓冲区中的内容不能被修改）
		readOnly();
		System.out.println("----------------------------------------\n");
	}
	
	
	/**
	 * byteBuffer普通api学习
	 * 	1. 创建ByteBuffer对象，并设置缓冲区大小：ByteBuffer.allocate(15)
	 * 	2. 添加数据api：buffer.put((byte)5);
	 * 	3. 对缓冲区的下标做归零操作：buffer.flip();
	 * 	4. 使用while循环读取数据：buffer.hasRemaining()，buffer.get()
	 * 	5. 清空数据:buffer.clear();
	 */
	public static void common(){
		//1. 创建byteBuffer
		ByteBuffer buffer = ByteBuffer.allocate(15);
		printlnBuffer(buffer,"创建的buffer对象：");
		
		//2. 添加4条数据
		buffer.put((byte)5);
		buffer.put((byte)2);
		buffer.put((byte)100);
		buffer.put((byte)20);
		printlnBuffer(buffer,"新增四条数据之后的buffer对象：");
		
		//3. 对缓冲区的下标做归零操作
		buffer.flip();
		printlnBuffer(buffer,"调用flip方法之后的buffer对象：");
		
		//4. 获取数据
		System.out.print("查询buffer中的数据:");
		while(buffer.hasRemaining()){
			System.out.print(buffer.get() + ",");
		}System.out.println();
		printlnBuffer(buffer,"查询数据结束之后的buffer对象：");
		
		//5. 清空数据
		buffer.clear();
		printlnBuffer(buffer,"清空buffer内容之后的buffer对象：");
	}
	
	/**
	 * 当读取过数据之后，需要重新读取的时候，可以使用要重新读取开始的标志位，
	 * 然后从标志位开始读取数据
	 */
	public static void reRead(){
		//1. 创建byteBuffer，并新增数据
		ByteBuffer buffer = ByteBuffer.allocate(15);
		buffer.put((byte)5);
		buffer.put((byte)2);
		buffer.put((byte)100);
		buffer.put((byte)20);
		printlnBuffer(buffer,"原始buffer对象：");
		
		//2. 读取数据，并设置标志位
		buffer.flip();	//刷新数据到可读状态
		System.out.println("查看设置标志位之前的数据："+buffer.get()+"，"+buffer.get());
		buffer.mark();	//在下标为2的时候设置标志位
		System.out.println("设置标志位的下标为："+buffer.position());
		System.out.println("查看设置标志位之后的数据："+buffer.get()+"，"+buffer.get());
		
		//3. 调用重置方法，将下标设置成标志位的值
		buffer.reset();
		printlnBuffer(buffer,"重置buffer对象之后的buffer对象：");
		
		//4. 读取标志位之后的数据
		System.out.println("读取标志位之后的数据：" + buffer.get() + "," + buffer.get());
	}
	
	/**
	 * 拷贝缓冲区
	 * 	不仅仅拷贝数据，而且将读取的状态。。。所有的都拷贝了
	 */
	public static void copyBuffer(){
		//1. 创建byteBuffer，并新增数据
		ByteBuffer buffer = ByteBuffer.allocate(15);
		buffer.put((byte)5);
		buffer.put((byte)2);
		buffer.put((byte)100);
		buffer.put((byte)20);
		printlnBuffer(buffer,"原始buffer对象：");
		
		//2. 读取前两条数据
		buffer.flip();	//刷新数据到可读状态
		System.out.println("查看前两条数据："+buffer.get()+"，"+buffer.get());
		printlnBuffer(buffer,"读取两条数据之后的buffer对象：");
		
		//3. 复制缓冲区
		ByteBuffer buffer2 = buffer.duplicate();
		printlnBuffer(buffer2, "复制缓冲区后的对象：");
		
		//4. 遍历复制后的buffer2中所有元素
		System.out.print("复制后继续读取buffer2中的元素：");
		while(buffer2.hasRemaining()){
			System.out.print(buffer2.get() +",");
		}
		System.out.println();
	}
	
	/**
	 * 将buffer的下标做归零操作
	 */
	public static void indexZeroBuffer(){
		//1. 创建byteBuffer，并新增数据
		ByteBuffer buffer = ByteBuffer.allocate(15);
		buffer.put((byte)5);
		buffer.put((byte)2);
		buffer.put((byte)100);
		buffer.put((byte)20);
		printlnBuffer(buffer,"原始buffer对象：");
		
		//2. 读取前两条数据
		buffer.flip();	//刷新数据到可读状态
		System.out.println("查看前两条数据："+buffer.get()+"，"+buffer.get());
		printlnBuffer(buffer,"读取两条数据之后的buffer对象：");
		
		//3. 将下标做归零操作
		buffer.rewind();
		
		//4. 遍历下标归零后的buffer中所有元素
		System.out.print("遍历下标归零后获取buffer中所有元素：");
		while(buffer.hasRemaining()){
			System.out.print(buffer.get() +",");
		}		
		System.out.println();
	}
	
	/**
	 * 创建子缓冲区，并将子缓冲区的数据与父级缓冲区的数据进行共享
	 */
	public static void subBuffer(){
		//1. 创建byteBuffer，并新增数据
		ByteBuffer buffer = ByteBuffer.allocate(15);
		buffer.put((byte)1);
		buffer.put((byte)2);
		buffer.put((byte)3);
		buffer.put((byte)4);
		buffer.put((byte)5);
		buffer.put((byte)6);
		printlnBuffer(buffer,"原始buffer对象：");
		
		//2. 创建子缓冲区 (子缓冲区是父级缓冲区中下标(2~4]的元素)
		buffer.position(2);
		buffer.limit(4);
		ByteBuffer subBuffer = buffer.slice();
		
		//3. 修改子缓冲区，并查询子缓冲区的数据
		System.out.print("查看子缓冲区中的所有数据：");
		while(subBuffer.hasRemaining()){
			subBuffer.put(subBuffer.position(),(byte) 100);		//修改数据
			System.out.print(subBuffer.get() +",");
		}		
		System.out.println();
		
		//4. 重置父级缓冲区，并查询父级缓冲区的数据
		//4.1 重置父级缓冲区
		buffer.position(0);
		buffer.limit(buffer.capacity());
		//4.2 查看缓冲区中的数据
		System.out.print("查看父级缓冲区中的所有数据：");
		while(buffer.hasRemaining()){
			System.out.print(buffer.get() +",");
		}		
		System.out.println();
	}
	
	/**
	 * 只读缓冲区（只读缓冲区中的内容不能被修改）
	 */
	public static void readOnly(){
		//1. 创建byteBuffer，并新增数据
		ByteBuffer buffer = ByteBuffer.allocate(15);
		buffer.put((byte)1);
		buffer.put((byte)2);
		buffer.put((byte)3);
		buffer.put((byte)4);
		buffer.put((byte)5);
		buffer.put((byte)6);
		printlnBuffer(buffer,"原始buffer对象：");
		
		//2. 创建只读缓冲区
		ByteBuffer readOnlyBuffer = buffer.asReadOnlyBuffer();
		
		//3. 查看只读缓冲区的内容
		System.out.print("查看只读缓冲区中的所有数据：");
		while(readOnlyBuffer.hasRemaining()){
//			readOnlyBuffer.put(readOnlyBuffer.position(),(byte) 100);		//修改数据
			System.out.print(readOnlyBuffer.get() +",");
		}		
		System.out.println();
	}	
	
	private static void printlnBuffer(ByteBuffer buffer,String str){
		System.out.println(str+"上限："+buffer.limit()+",容量："+buffer.capacity()+",下标："+buffer.position());
	}
	
}

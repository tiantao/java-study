package com.tiantao.hight.optimize.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * 使用nio对文件进行操作
 * @author tiantao
 *
 */
public class NIO {
	
	public static void main(String[] args) {
//		//1. 使用NIO拷贝文件
//		nioCopyFile("E:\\软件\\JDK\\jdk-8u73-windows-x64.exe", 	//源文件地址
//			"E:\\wuyong\\删除\\jdk-8u73-windows-x64.exe");	//目标文件地址
//		
//		//2. 对文件进行写入操作（使用内存映射的方式）
		write("E:\\wuyong\\删除\\bbb.txt", "123345567");
		
		//3. 读取文件内容操作（使用内存映射的方式）
		read("E:\\wuyong\\删除\\bbb.txt");
		
		//4. 聚合写(将多个bytebuffer合并成一个数组，写入一个文件)
//		gatherWrite("E:\\wuyong\\删除\\gatherWrite.txt",false);
		
		//5. 分散读（将多个buffer聚合成一个buffer数组，使用buffer数组读取文件中的内容）
//		disperseRead("E:\\wuyong\\删除\\gatherWrite.txt");
	}
	
	/**
	 * 使用nio拷贝文件
	 * @param resource	源文件路径
	 * @param destination	目标文件路径
	 */
	public static void nioCopyFile(String resource,String destination){
		FileChannel readChannel = null;
		FileChannel writeChannel = null;
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			//1. 创建文件流对象
			fis = new FileInputStream(resource);		//输入流
			fos = new FileOutputStream(destination);	//输出流
			
			//2. 创建文件通道
			readChannel = fis.getChannel();		//创建文件读取通道
			writeChannel = fos.getChannel();	//创建文件写入通道
			
			//3. 创建缓冲区
			ByteBuffer buffer = ByteBuffer.allocate(1024 * 8);
			
			//4. 拷贝文件
			while(true){
				buffer.clear();
				int len = readChannel.read(buffer);		//读取数据
				if(len != -1){	//如果还能读取到数据
					buffer.flip();
					writeChannel.write(buffer);		//写入数据
				}else{
					break;
				}
			}
		} catch (FileNotFoundException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace();
		}finally {
			try {
				if(fis != null) fis.close();
				if(fos != null) fos.close();
				if(writeChannel != null) writeChannel.close();
				if(readChannel != null) readChannel.close();
			} catch (IOException e) { e.printStackTrace(); }
		}
	}
	
	/**
	 * 将字符串写入指定的路径中（使用内存映射的方式）
	 * @param path	要写入的路径
	 * @param str	要写入的字符串
	 */
	public static void write(String path,String str){
		RandomAccessFile raf = null;
		FileChannel fc = null;
		try {
			//1. 创建RandomAccessFile对象（该对象同时支持文件的读写）
			//  第二个参数：r：代表读，w：代表写，rw：代表读写，还有其他的，具体可以看源码
			raf = new RandomAccessFile(path, "rw");
			
			//2. 创建管道
			fc = raf.getChannel();
			
			//3. 创建可映射的读写缓冲区，读写范围是：0~1024 * 15
			MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_WRITE, 0, 1024 * 15);
			
			//4. 修改mbb中的内容
			mbb.put(str.getBytes());
			
		} catch (FileNotFoundException e) {e.printStackTrace();
		} catch (IOException e) {e.printStackTrace();
		}finally {
			try {
				if(raf != null) raf.close();
				if(fc != null) fc.close();
			} catch (IOException e) {e.printStackTrace();}
		}
	}
	
	/**
	 * 读取文件中的内容（使用内存映射的方式）
	 * @param path	要写入的路径
	 */
	public static void read(String path){
		RandomAccessFile raf = null;
		FileChannel fc = null;
		try {
			//1. 创建RandomAccessFile对象（该对象同时支持文件的读写）
			//  第二个参数：r：代表读，w：代表写，rw：代表读写，还有其他的，具体可以看源码
			raf = new RandomAccessFile(path, "r");
			
			//2. 创建管道
			fc = raf.getChannel();
			
			//3. 创建可映射的读写缓冲区，读取返回：0~fc.size(该文件的大小)
			MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
			
			//4. 读取mbb中的内容
			while(mbb.hasRemaining()){
				System.out.print(mbb.get()+",");
			}
			
		} catch (FileNotFoundException e) {e.printStackTrace();
		} catch (IOException e) {e.printStackTrace();
		}finally {
			try {
				if(raf != null) raf.close();
				if(fc != null) fc.close();
			} catch (IOException e) {e.printStackTrace();}
		}
	}
	
	/**
	 * 聚合写(将多个bytebuffer合并成一个数组，写入一个文件)
	 * @param path
	 * @param isAppend	是否追加
	 */
	public static void gatherWrite(String path,boolean isAppend){
		FileOutputStream fos = null;
		FileChannel fc = null;
		try {
			//1. 创建两个buffer 
			ByteBuffer buffer1 = ByteBuffer.wrap("java性能优化".getBytes());
			ByteBuffer buffer2 = ByteBuffer.wrap("小明同学".getBytes());
			
			//2. 将两个buffer合并成一个buffer数组
			ByteBuffer[] bufs = new ByteBuffer[]{buffer1,buffer2};
			
			//3. 创建文件对象，如果该文件不存在，那么创建该文件
			File file = new File(path);
			if(!file.exists()){
				file.createNewFile();
			}
			
			//4. 创建输出流，并将数据通过channel写入文件中
			fos = new FileOutputStream(path, isAppend);
			fc = fos.getChannel();
			fc.write(bufs);
			
		} catch (IOException e) {e.printStackTrace();
		} finally {
			try {
				if(fos != null) fos.close();
				if(fc != null) fc.close();
			} catch (IOException e) {e.printStackTrace();}
		}
	}
	
	/**
	 * 分散读（将多个buffer聚合成一个buffer数组，使用buffer数组读取文件中的内容）
	 * @param path
	 */
	public static void disperseRead(String path){
		FileInputStream fis = null;
		FileChannel fc = null;
		try {
			//1. 创建两个buffer 
			ByteBuffer buffer1 = ByteBuffer.allocate(15);
			ByteBuffer buffer2 = ByteBuffer.allocate(15);
			
			//2. 将两个buffer合并成一个buffer数组
			ByteBuffer[] bufs = new ByteBuffer[]{buffer1,buffer2};
			
			//3. 创建输入流对象,并获取channel
			fis = new FileInputStream(path);
			fc = fis.getChannel();
			
			//4. 通过buffer数组读取数据
			fc.read(bufs);
			
			//5. 读取buffer中的内容
			System.out.println(new String(bufs[0].array()));
			System.out.println(new String(bufs[1].array()));
			
		} catch (IOException e) {e.printStackTrace();
		} finally {
			try {
				if(fis != null) fis.close();
				if(fc != null) fc.close();
			} catch (IOException e) {e.printStackTrace();}
		}
	}
	
}

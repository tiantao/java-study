package com.tiantao.hight.optimize.string;

import java.util.ArrayList;
import java.util.List;

/**
 * 字符串的内存溢出
 * @author tiantao
 */
public class SubStringMemoryOverflow {
	public static void main(String[] args) {
		
		//1. 很容易内存溢出（JDK1.8之后好像没有了！）
		//原因是str.substring(1, 100)这个方法里面的value值是共享的，
		List<String> hander = new ArrayList<String>();
		for (int i = 0; i < 1000000; i++) {
			String str = new String(new char[10000]);
			hander.add(str.substring(1, 100));
		}
		
		//2. 使用这种方式不容易出现内存溢出
		List<String> hander1 = new ArrayList<String>();
		for (int i = 0; i < 100000; i++) {
			String str = new String(new char[1000000]);
			hander1.add(new String(str.substring(1, 100)));
		}
		
	}
}



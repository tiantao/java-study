package com.tiantao.hight.optimize.collections;

import java.util.TreeMap;

/**
 * 使用TreeMap对数据进行自定义排序
 * 	通过实现key的排序接口，从而实现TreeMap对数据的排序
 * @author tiantao
 *
 */
public class TreeMapSort {
	public static void main(String[] args) {
		
		//通过分数对数据进行排序
		TreeMap<Student, String> map = new TreeMap<Student, String>();
		map.put(new Student("张三", 96), "张三");
		map.put(new Student("李四", 78), "李四");
		map.put(new Student("王五", 85), "王五");
		map.put(new Student("赵六", 98), "赵六");
		
		System.out.println(map.keySet());
	}
}

//实现排序接口
class Student implements Comparable<Student>{
	private String name;
	private Integer store;
	public Student(String name, Integer store) {
		this.name = name;
		this.store = store;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStore() {
		return store;
	}
	public void setStore(Integer store) {
		this.store = store;
	}
	//实现排序方法
	public int compareTo(Student o) {
		if(this.store < o.store){
			return 1;
		}else if(this.store > o.store){
			return -1;
		}else{
			return 0;
		}
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", store=" + store + "]\n";
	}
}


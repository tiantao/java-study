package com.tiantao.hight.optimize.collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * ArrayList和LinkedList的比较(ArrayList和LinkedList都是线程非安全的)
 * 	性能测试结果：
 *    arrayList：从尾部添加所需要的时间29ms
 *    linkedList：从尾部添加所需要的时间27ms
 *    arrayList：从尾部删除数据所需要的时间4ms
 *    linkedList：从尾部删除数据所需要的时间3ms
 *    	
 *    arrayList：从中间添加所需要的时间2817ms
 *    linkedList：从中间添加所需要的时间4ms
 *    arrayList：从中部删除数据所需要的时间266ms
 *    linkedList：从中部删除数据所需要的时间14499ms
 *    arrayList：从头部添加所需要的时间3016ms
 *    linkedList：从头部添加所需要的时间4ms
 *    arrayList：从头部删除数据所需要的时间596ms
 *    linkedList：从头部删除数据所需要的时间1ms
 *    	
 *    arrayList：是用foreach遍历数据所需要的时间9ms
 *    linkedList：是用foreach遍历数据所需要的时间22ms
 *    	
 *    arrayList：是用for遍历数据所需要的时间9ms
 *    linkedList：是用for遍历数据所需要的时间387123ms
 *  性能测试总结：
 *    使用linkedList时：
 *    	1. 从头部，中间，尾部添加元素效率较高，
 *    	2. 从头部，尾部删除元素效率较高，但是从中间删除元素效率非常低下（这时需要考虑使用ArrayList）
 *    	3. 使用foreach遍历，不要使用for遍历，否则会非常慢
 *    使用ArrayList时：
 *    	1. 从头部，中间添加，删除元素效率较低
 *    	2. 从尾部添加，删除效率较高
 *    	3. 使用for和foreach遍历都可以，效率差不多
 *    
 * @author tiantao
 */
public class ArrayAndLinkedCompare {
	public static void main(String[] args) {
		List<String> arrayList = new ArrayList<String>();
		List<String> linkedList = new LinkedList<String>();
		
		//1.1 测试ArrayList从末尾添加元素的效率						//29ms
		addTail(arrayList, "arrayList");
		//1.2 测试从LinkedList从末尾添加元素的效率					//27ms
		addTail(linkedList, "linkedList");
		//1.3 测试ArrayList从末尾删除元素的效率						//4ms
		delTail(arrayList, "arrayList");
		//1.2 测试从LinkedList从末尾删除元素的效率					//3ms
		delTail(linkedList, "linkedList");
		System.out.println();
		
		//2.1 测试ArrayList从中间添加元素的效率					//2817ms
		addMiddle(arrayList, "arrayList");
		//2.2 测试LinkedList从中间添加元素的效率					//4ms
		addMiddle(linkedList, "linkedList");
		//2.3 测试ArrayList从中间删除元素的效率					//266ms
		delMiddle(arrayList, "arrayList");
		//2.4 测试LinkedList从中间删除元素的效率					//14499ms
		delMiddle(linkedList, "linkedList");
		//2.5 测试ArrayList从头部添加元素的效率					//3016ms
		addFirst(arrayList, "arrayList");
		//2.6 测试LinkedList从头部添加元素的效率					//4ms
		addFirst(linkedList, "linkedList");
		//2.7 测试ArrayList从头部删除元素的效率					//596ms
		delFirst(arrayList, "arrayList");
		//2.8 测试LinkedList从头部删除元素的效率					//1ms
		delFirst(linkedList, "linkedList");
		System.out.println();
		
		//3.1 测试使用foreach遍历ArrayList的效率					//9ms
		foreachTraverse(arrayList, "arrayList");
		//3.2 测试使用foreach遍历LinkedList的效率					//22ms
		foreachTraverse(linkedList, "linkedList");
		System.out.println();
		
		//4.1 测试使用for遍历ArrayList的效率					//9ms
		forTraverse(arrayList, "arrayList");
		//4.2 测试使用for遍历LinkedList的效率					//
		forTraverse(linkedList, "linkedList");
	}
	
	/**
	 * 从尾部添加数据
	 * @param name
	 */
	public static void addTail(List<String> list,String name) {
		long begin = System.currentTimeMillis();
		for (int i = 0; i < 500000; i++) {
			list.add(String.valueOf(i));
		}
		long end = System.currentTimeMillis();
		System.out.println(name + "：从尾部添加所需要的时间" + (end - begin) + "ms");
	}

	/**
	 * 从头部添加数据
	 * @param name
	 */
	public static void addFirst(List<String> list,String name) {
		long begin = System.currentTimeMillis();
		for (int i = 0; i < 50000; i++) {
			list.add(0, String.valueOf(i));
		}
		long end = System.currentTimeMillis();
		System.out.println(name + "：从头部添加所需要的时间" + (end - begin) + "ms");
	}

	
	/**
	 * 从中间添加数据
	 * @param name
	 */
	public static void addMiddle(List<String> list,String name) {
		long begin = System.currentTimeMillis();
		for (int i = 0; i < 50000; i++) {
			list.add(1, String.valueOf(i));
		}
		long end = System.currentTimeMillis();
		System.out.println(name + "：从中间添加所需要的时间" + (end - begin) + "ms");
	}
	
	
	/**
	 * 从尾部删除数据
	 * @param name
	 */
	public static void delTail(List<String> list,String name) {
		long begin = System.currentTimeMillis();
		for (int i = 0; i < 100000; i++) {
			list.remove(list.size() - 1);
		}
		long end = System.currentTimeMillis();
		System.out.println(name + "：从尾部删除数据所需要的时间" + (end - begin) + "ms");
	}

	/**
	 * 从头部删除数据
	 * @param list
	 * @param name
	 */
	public static void delFirst(List<String> list,String name) {
		long begin = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			list.remove(0);
		}
		long end = System.currentTimeMillis();
		System.out.println(name + "：从头部删除数据所需要的时间" + (end - begin) + "ms");
	}

	/**
	 * 从中部删除数据
	 * @param name
	 */
	public static void delMiddle(List<String> list,String name) {
		long begin = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			list.remove(list.size()>>1);		//>> :相当于除以2
		}
		long end = System.currentTimeMillis();
		System.out.println(name + "：从中部删除数据所需要的时间" + (end - begin) + "ms");
	}
	
	/**
	 * 使用foreach遍历数据
	 * @param list
	 * @param name
	 */
	@SuppressWarnings("unused")
	public static void foreachTraverse(List<String> list,String name){
		long begin = System.currentTimeMillis();
		String tmp;
		for (String str : list) {
			tmp = str;
		}
		long end = System.currentTimeMillis();
		System.out.println(name + "：是用foreach遍历数据所需要的时间" + (end - begin) + "ms");
	}
	
	/**
	 * 使用for遍历数据
	 * @param list
	 * @param name
	 */
	@SuppressWarnings("unused")
	public static void forTraverse(List<String> list,String name){
		long begin = System.currentTimeMillis();
		String tmp;
		for (int i = 0; i < list.size(); i++) {
			tmp = list.get(i);
		}
		long end = System.currentTimeMillis();
		System.out.println(name + "：是用for遍历数据所需要的时间" + (end - begin) + "ms");
	}
	
}

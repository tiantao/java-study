package com.tiantao.hight.optimize.string;

/**
 * String 字符串拼接
 * 	字符串拼接尽量使用StringBuilder和Stringbuffer
 * 	StringBuilder和StringBuffer的区别：
 * 		StringBuilder没有做线程同步，是线程不安全的，效率稍微高一点
 * 		StringBuffer做了线程同步，是线程安全的，效率稍微低一点
 * 	注意：在创建StringBuilder和StringBuffer对象的时候，加上容量，可以适当提高执行效率，有可能提高50%，
 * 			但是这个很难调节，而且要确定字符串的大小
 * @author tiantao
 */
public class StringJoin {
	public static void main(String[] args) {
		//1. 正常拼接字符串
		long begin = System.currentTimeMillis();
		String str = "";
		for (int i = 0; i < 10000; i++) {
			str = str + i;
		}
		long end = System.currentTimeMillis();
		System.out.println("+所花费的时间："+(end - begin));
		
		//2. 使用concat拼接字符串
		long beginConcat = System.currentTimeMillis();
		String strConcat = "";
		for (int i = 0; i < 10000; i++) {
			strConcat = strConcat.concat(String.valueOf(i));
		}
		long endConcat = System.currentTimeMillis();
		System.out.println("Concat所花费的时间："+(endConcat - beginConcat));
		
		//3. 使用StringBuilder拼接字符串
		long beginBuilder = System.currentTimeMillis();
		StringBuilder sbBuilder = new StringBuilder();
		for (int i = 0; i < 10000000; i++) {
			sbBuilder.append(String.valueOf(i));
		}
		long endBuilder = System.currentTimeMillis();
		System.out.println("StringBuilder所花费的时间："+(endBuilder - beginBuilder));
		
		//4. 使用StringBuffer拼接字符串
		long beginBuffer = System.currentTimeMillis();
		StringBuffer sbBuffer = new StringBuffer();
		for (int i = 0; i < 10000000; i++) {
			sbBuffer.append(String.valueOf(i));
		}
		long endBuffer = System.currentTimeMillis();
		System.out.println("StringBuffer所花费的时间："+(endBuffer - beginBuffer));
		
	}
}

package com.tiantao.hight.optimize.other;

/**
 * 使用arrayCopy方法，拷贝数组
 * 		拷贝的数组每个元素越复杂，arrayCopy的效率越高
 * 		如果数组每个元素的类型是int，那么使用for效率稍微偏高一点点
 * 		如果是字符串，那么arrayCopy的效率要高50%，如果更复杂，则效率更高
 * @author tiantao
 *
 */
public class ArrayCopy {
	public static void main(String[] args) {
		
		//1. 创建数组
		final int length = 90000;
		String[] array = new String[length];
		for (int i = 0; i < array.length; i++) {
			array[i] = String.valueOf(i);
		}
		
		//2. 使用普通方式拷贝数组
		long start = System.currentTimeMillis();
		for (int j = 0; j < 100; j++) {
			String[] array1 = new String[array.length];
			for (int i = 0; i < array1.length; i++) {
				array1[i] = array[i];
			}
		}
		System.out.println("普通方式拷贝数组:"+(System.currentTimeMillis() - start));
		
		//3. 使用arrayCopy方法，拷贝数组
		start = System.currentTimeMillis();
		for (int j = 0; j < 100; j++) {
			String[] array2 = new String[array.length];
			System.arraycopy(array,0,array2,0,length);
		}
		System.out.println("使用arrayCopy方法,拷贝数组:"+(System.currentTimeMillis() - start));
		
		
		
		
		
		
		
	}
}

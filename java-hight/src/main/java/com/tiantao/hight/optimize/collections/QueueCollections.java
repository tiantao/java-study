package com.tiantao.hight.optimize.collections;

import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 队列
 * @author tiantao
 *
 */
public class QueueCollections {
	public static void main(String[] args) {
		//1. 线程安全，先入先出队列
//		concurrentLinkedQueue();
		
		//2. 阻塞队列
		blockQueue();
	}
	
	/**
	 * ConcurrentLinkedQueue（先进先出队列，线程安全）
	 * 		是一个基于连接点的无边界的线程安全队列，他采用FiFo袁总对元素进行排序
	 * 		最先插入的元素最先被删除，最后插入的元素最后被删除，	
	 */
	public static void concurrentLinkedQueue(){
		//1. 创建队列
		Queue<String> queue = new ConcurrentLinkedQueue<String>();
		
		//2. 往队列中添加元素
		queue.offer("a");
		queue.offer("b");
		System.out.println("创建队列，并在队列中添加元素："+queue);

		//3. 返回第一个元素，但不删除该元素
		System.out.println("返回第一个元素，但不删除该元素："+queue.element());
		
		//4. 返回第一个元素，并在队列中删除该元素
		System.out.println("删除第一个元素："+queue.poll());
		
		//5. 返回第一个元素，但不删除该元素
		System.out.println("返回第一个元素，但不删除该元素："+queue.peek());
		
		//6. 获取队列的大小
		System.out.println("获取队列的大小:"+queue.size());
		
		//7. 查询该队列是否为空队列
		System.out.println("查询该队列是否为空队列"+queue.isEmpty());
		
		//8. 如果该队列为空队列，那么获取第一个元素的值为null
		queue.poll();
		System.out.println("如果该队列为空队列，那么获取第一个元素的值:"+queue.poll());
	}
	
	/**
	 * 阻塞队列
	 * 	  阻塞队列是在普通队列的基础上又支持了如下两个附加操作
	 * 	  1. 支持阻塞的插入方法：队列满时，队列会阻塞插入元素的线程，知道队列不满，然后插入元素
	 * 	  2. 支持阻塞的移除方法：队列空时，获取元素的线程会等待队列变成非空，然后移除元素。
	 */
	public static void blockQueue(){
		try {
			//1. 创建队列(还有其他的类实现方式，具体去百度查：阻塞队列)
			//	LinkedBlockingQueue:先进先出的队列
			BlockingQueue<String> queue = new LinkedBlockingQueue<String>(3);	//设置队列的长度为3
			
			//2. 插入元素
			queue.put("a");
			queue.put("b");
			queue.put("c");
//			queue.put("d");		//由于该队列只能存三个元素，当存第四个元素的时候线程会阻塞
			queue.offer("d");	//使用该方法，只会让该元素添加不进去，但线程不会阻塞
			
			//3. 获取第一个元素，并移除该元素
			System.out.println(queue.take());
			System.out.println(queue.take());
//			System.out.println(queue.take());
//			System.out.println(queue.take());	//由于该队列中只有三个元素，如果要删除第4个的时候，会出现线程阻塞
//			System.out.println(queue.poll());	//使用该方法，只会返回null，线程不会阻塞
			
			//4. 队列的长度
			System.out.println("队列的长度："+queue.size());
			
			//5. 查询该队列是否为空队列
			System.out.println("查询该队列是否为空队列"+queue.isEmpty());
			
			//6. 返回第一个元素，但不删除该元素
			System.out.println("返回第一个元素，但不删除该元素"+queue.peek());
			System.out.println("返回第一个元素，但不删除该元素"+queue.element());
			System.out.println(queue);
			
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

package com.tiantao.hight.design.mode;

import java.util.HashMap;
import java.util.Map;

/**
 * 享元模式
 * 	目的：提高程序性能
 *  核心思想：如果一个系统中存在多个相同的对象，那么只需要共享一份对象的拷贝，
 *  		而不是每次使用都创建新的对象
 *  
 * @author tiantao
 *
 */
public class Flyweight {
	public static void main(String[] args) {
		FlyweightFactory flyFactory = new FlyweightFactory();
		IFlyweight factorya = flyFactory.factory("a");
		IFlyweight factoryb = flyFactory.factory("b");
		IFlyweight factoryc = flyFactory.factory("c");
		
		factorya.operation("d");
		factoryb.operation("e");
		factoryc.operation("f");
	}
}

/**
 * 1. 创建享元接口
 * @author tiantao
 */
interface IFlyweight{
	//一个示意性方法，参数state是外蕴状态
	public void operation(String state);
}

/**
 * 享元类
 * @author tiantao
 *
 */
class ConcreteFlyweight implements IFlyweight{
	private String intrinsicState = null;
	public ConcreteFlyweight(String intrinsicState) {
		this.intrinsicState = intrinsicState;
	}
	public void operation(String state) {
		System.out.print("对象的key = " + this.intrinsicState);
        System.out.println("\t 传入的参数 = " + state);
	}
}

/**
 * 享元类工厂类
 * @author tiantao
 *
 */
class FlyweightFactory{
	private Map<String,IFlyweight> flws = new HashMap<String, IFlyweight>();
	/**
	 * 获取享元类
	 * @param state
	 * @return
	 */
	public IFlyweight factory(String state){
		//从缓存中查找对象
		IFlyweight fly = flws.get(state);
		if(fly == null){
			fly = new ConcreteFlyweight(state);
			flws.put(state, fly);
		}
		return fly;
	}
}


























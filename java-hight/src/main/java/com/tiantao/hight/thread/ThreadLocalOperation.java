package com.tiantao.hight.thread;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ThreadLocal
 * 	 理解：
 * 		ThreadLocal为变量在每个线程中都创建了一个副本，那么每个线程可以访问自己内部的副本变量
 * 		
 * @author tiantao
 *
 */
public class ThreadLocalOperation {
	public static void main(String[] args) {
		//1. 创建线程池
		ExecutorService threadPool = Executors.newCachedThreadPool();
		
		//2. 创建第一个线程，
		threadPool.execute(new Runnable() {
			public void run() {
				DateUtils.addSimple("yyyy-MM-dd");
				DateUtils.addSimple("yyyy-MM-dd HH:mm:ss");
				DateUtils.addSimple("yyyy/MM/dd");
				System.out.println(Thread.currentThread().getName() + ":" + DateUtils.getThreads().get());
			}
		});
		
		//创建第二个线程
		threadPool.execute(new Runnable() {
			public void run() {
				DateUtils.addSimple("yyyy-MM-dd");
				DateUtils.addSimple("yyyy/MM/dd");
				System.out.println(Thread.currentThread().getName() + ":" + DateUtils.getThreads().get());
			}
		});
		
		// 创建第三个线程
		threadPool.execute(new Runnable() {
			public void run() {
				DateUtils.addSimple("yyyy-MM-dd");
				DateUtils.addSimple("yyyy-MM-dd HH:mm:ss");
				System.out.println(Thread.currentThread().getName() + ":" + DateUtils.getThreads().get());
			}
		});
		
		
		DateUtils.addSimple("yyyy-MM-dd");
		System.out.println(Thread.currentThread().getName() + ":" + DateUtils.getThreads().get());
		
	}
}

class DateUtils{
	private static ThreadLocal<Map<String,SimpleDateFormat>> threads = new ThreadLocal<Map<String,SimpleDateFormat>>();
	
	/**
	 * 添加一个SimpleDateFormat对象
	 * @param template
	 */
	public static void addSimple(String template){
		Map<String, SimpleDateFormat> map = threads.get();
		if(map == null){
			threads.set(new HashMap<String, SimpleDateFormat>());
		}
		Set<String> keys = threads.get().keySet();
		if(keys.contains(template)){
			return;
		}else{
			threads.get().put(template, new SimpleDateFormat(template));
		}
	}

	public static ThreadLocal<Map<String, SimpleDateFormat>> getThreads() {
		return threads;
	}
	
	
}

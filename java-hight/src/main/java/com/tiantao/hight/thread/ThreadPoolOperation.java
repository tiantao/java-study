package com.tiantao.hight.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * 线程池的使用
 * 	  newCachedThreadPool：创建一个可缓存的线程池，如果线程池过长，可以自动回收线程，如果线程池中线程太少，则自动新建线程
 * 	  newFixedThreadPool：创建定长线程池，可控制线程最大并发数，超出的线程会在队列中等待
	  newScheduledThreadPool：创建一个定长线程池，并定时执行任务
	  newSingleThreadExecutor：创建一个单线程化的线程池，它只会执行完一个任务后再执行下一个任务，并保证所有任务按照指定顺序（FIFO，LIFO，优先级）执行
 * @author tiantao
 *
 */
public class ThreadPoolOperation {
	public static void main(String[] args) throws InterruptedException {
		//1. 创建一个可缓存的线程池，如果线程池过长，可以自动回收线程，如果线程池中线程太少，则自动新建线程
//		cachedThreadPool();
		
		//2. 创建定长线程池，可控制线程最大并发数，超出的线程会在队列中等待
//		fixedThreadPool();
		
		//3. 创建一个定长线程池，并定时执行任务
//		scheduledThreadPool();
		
		//4. 创建一个单线程化的线程池，它只会执行完一个任务后再执行下一个任务，并保证所有任务按照指定顺序（FIFO，LIFO，优先级）执行
		singleThreadExecutor();
		
	}
	/**
	 * 创建一个可缓存的线程池，如果线程池过长，可以自动回收线程，如果线程池中线程太少，则自动新建线程
	 * @throws InterruptedException 
	 */
	public static void cachedThreadPool() throws InterruptedException{
		//1. 创建一个线程池
		ExecutorService threadPool = Executors.newCachedThreadPool();
		
		//2. 使用线程池中的线程执行任务
		for (int i = 0; i < 30; i++) {
			threadPool.execute(new Task("params"+i));
			System.out.println("线程池信息："+threadPool.toString());
		}
		
		Thread.sleep(100000L);
		//3. 查看线程池信息
		System.out.println("线程池信息："+threadPool.toString());//等待一段时间后，刚刚创建的30个线程都被自动回收了
	}
	
	/**
	 * 创建定长线程池，可控制线程最大并发数，超出的线程会在队列中等待
	 */
	public static void fixedThreadPool(){
		//1. 创建线程池
		ExecutorService threadPool = Executors.newFixedThreadPool(5);
		
		//2. 使用线程池中的线程执行任务
		for (int i = 0; i < 30; i++) {
			threadPool.execute(new Task("params"+i));
			System.out.println("线程池信息："+threadPool.toString());
		}
	}
	/**
	 *  创建一个定长线程池，并定时执行任务
	 */
	public static void scheduledThreadPool(){
		//1. 创建线程池
		ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(5);
		
		//2. 三秒之后执行该任务执行任务
		threadPool.schedule(new Task("params1"), 3, TimeUnit.SECONDS);
		
		//3. 1秒后每三秒执行一次
		threadPool.scheduleAtFixedRate(new Task("params2"),1, 3, TimeUnit.SECONDS);
	}
	/**
	 * 创建一个单线程化的线程池，它只会执行完一个任务后再执行下一个任务，并保证所有任务按照指定顺序（FIFO，LIFO，优先级）执行
	 */
	public static void singleThreadExecutor(){
		//1. 创建线程池
		ExecutorService threadPool = Executors.newSingleThreadExecutor();
		
		//2. 执行任务
		for (int i = 0; i < 30; i++) {
			threadPool.execute(new Task("params"+i));
		}
		
	}
}
/**
 * 要执行的任务
 */
class Task extends Thread{
	private String params;
	public Task(String params) {
		this.params = params;
	}
	@Override
	public void run() {
		System.out.println("["+com.tiantao.common.utils.DateUtils.getDate(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss.SSS")+"]  "+
						Thread.currentThread().getName()+"执行任务，params："+params);
		try {
			Thread.sleep(1000L);
		} catch (InterruptedException e) {e.printStackTrace();}
	}
}





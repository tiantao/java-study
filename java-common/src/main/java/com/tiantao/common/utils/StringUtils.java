package com.tiantao.common.utils;

import java.util.UUID;

public class StringUtils {
	
	
	/**
	 * 获取一个永远不会重复的字符串
	 * @return
	 */
	public static String uuID(){
		return UUID.randomUUID().toString().replace("-", "").toUpperCase();
	}
	
	/**
	 * 校验一个字符串是否为 "" 或者为 null
	 * 
	 * StringUtils.isEmpty(null)      => true
     * StringUtils.isEmpty("")        => true
     * StringUtils.isEmpty(" ")       => false
     * StringUtils.isEmpty("bob")     => false
     * StringUtils.isEmpty("  bob  ") => false	
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str){
		return str == null || str.length() == 0 ;
	}
	
	/**
	 * 
	 * 校验一个字符串是否不为 "" 或者为 null
	 * 
	 * StringUtils.isNotEmpty(null)      = false
     * StringUtils.isNotEmpty("")        = false
     * StringUtils.isNotEmpty(" ")       = true
     * StringUtils.isNotEmpty("bob")     = true
     * StringUtils.isNotEmpty("  bob  ") = true
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(String str){
		return str != null && str.length() > 0;
	}
	
	/**
	 * 校验一个字符串是否为 "" 或者为 null
	 * 
	 * StringUtils.isBlank(null)      = true
     * StringUtils.isBlank("")        = true
     * StringUtils.isBlank(" ")       = true
     * StringUtils.isBlank("bob")     = false
     * StringUtils.isBlank("  bob  ") = false
     * 
	 * @param str
	 * @return
	 */
	public static boolean isBlank(String str){
		//1.如果str为"",获取为null，返回true
		if(isEmpty(str)){
			return true;
		}
		
		//2.如果是" ",也要返回false
		for (char cr : str.toCharArray()) {
			if(Character.isWhitespace(cr) == false){   //只要该字符串中有一个char是非空格的字段，就返回false
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 校验一个字符串是否不为 "" 或者不为 null
     * StringUtils.isNotBlank(null)      = false
     * StringUtils.isNotBlank("")        = false
     * StringUtils.isNotBlank(" ")       = false
     * StringUtils.isNotBlank("bob")     = true
     * StringUtils.isNotBlank("  bob  ") = true
	 * @param str
	 * @return
	 */
	public static boolean isNotBlank(String str){
		//1.如果str为"",获取为null，返回false
		if(isEmpty(str)){
			return false;
		}
		
		//2.如果是" ",也要返回true
		for (char cr : str.toCharArray()) {
			if(Character.isWhitespace(cr) == false){   //只要该字符串中有一个char是非空格的字段，就返回false
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * 拼接字符串
	 * @param prefix		前缀匹配
	 * @param partition		分割符
	 * @param suffix		后缀匹配
	 * @param strs			要拼接的数组
	 * @return
	 */
	public static String join(String prefix,String partition,String suffix,String...strs){
		StringBuffer sb = new StringBuffer();
		//1.拼接前缀
		if(StringUtils.isNotBlank(prefix)){   //如果前缀不是空
			sb.append(prefix);
		}
		
		//2.拼接中间的
		if(strs.length != 0){
			sb.append(strs[0]);
			if(StringUtils.isNotBlank(partition)){
				for (int i = 1; i < strs.length; i++) {
					sb.append(partition);
					sb.append(strs[i]);
				}
			}else{
				for (int i = 1; i < strs.length; i++) {
					sb.append(strs[i]);
				}
			}
		}
		
		//3.拼接后缀
		if(StringUtils.isNotBlank(suffix)){
			sb.append(suffix);
		}
		
		return sb.toString();
	}

	/**
	 * 拼接所有字符串
	 * @param strs
	 * @return
	 */
	public static String joinStrs(String...strs){
		StringBuilder sb = new StringBuilder();
		for (String str : strs) {
			sb.append(str);
		}
		return sb.toString();
	}
	
	
}

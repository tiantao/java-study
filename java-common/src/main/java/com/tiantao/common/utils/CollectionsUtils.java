package com.tiantao.common.utils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * 针对map的操作
 * 
 * @author tiantao 
 * @version 1.0
 */
public class CollectionsUtils {
	
	/**
	 * 创建一个HashMap
	 * @param obj 必须是偶数个
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <K,V>  Map<K,V> createHashMap(Object...obj){
		Map<K,V> map = new HashMap<K, V>();
		for (int i = 0; i < obj.length; i+=2) {
			map.put((K)obj[i], (V)obj[i+1]);
		}
		return map;
	}
	
	/**
	 * 创建一个TreeMap
	 * @param obj 必须是偶数个
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <K,V>  TreeMap<K,V> createTreeMap(Object...obj){
		TreeMap<K,V> map = new TreeMap<K, V>();
		for (int i = 0; i < obj.length; i+=2) {
			map.put((K)obj[i], (V)obj[i+1]);
		}
		return map;
	}
	
	/**
	 * 创建一个LinkedHashMap
	 * @param obj 必须是偶数个
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <K,V> LinkedHashMap<K,V> linkedHashMap(Object...obj){
		LinkedHashMap<K,V> map = new LinkedHashMap<K, V>();
		for (int i = 0; i < obj.length; i+=2) {
			map.put((K)obj[i], (V)obj[i+1]);
		}
		return map;
	}
	
	/**
	 * 在list集合中忽略大小写寻找元素，如果找到返回true，否则返回false
	 * @param list
	 * @param str	要寻找的元素
	 * @return
	 */
	public static boolean containsIgnoreCase(List<String> list,String str){
		for (String s : list) {
			if(s.equalsIgnoreCase(str)){
				return true;
			}
		}
		return false;
	}
	
}

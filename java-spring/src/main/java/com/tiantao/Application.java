package com.tiantao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * spring boot 启动类
 * @author tiantao
 *
 */
@SpringBootApplication
@EnableAspectJAutoProxy(exposeProxy=true)
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}

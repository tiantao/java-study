package com.tiantao.spring.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tiantao.spring.aop.Z_PointCutAnnotationTest;
import com.tiantao.spring.aop.Z_PointCutSameClassTest;
import com.tiantao.spring.aop.Z_PointCutTest;

/**
 * 用于测试aop
 * @author tiantao
 *
 */
@RestController
@RequestMapping("/aop")
public class SpringbootAopController {
	
	@Autowired
	private Z_PointCutTest pointCut;
	@Autowired
	private Z_PointCutAnnotationTest pointCutAnnotation;
	@Autowired
	private Z_PointCutSameClassTest pointCutSameClass;
	/**
	 * 普通aop测试
	 */
	@RequestMapping("/common")
	public void common(){
//		pointCut.testMap(CollectionsUtils.createHashMap("K1","V1"));
		pointCut.testResultMap("111111111", "22222222222222");
	}
	
	/**
	 * 将aop与注解关联使用
	 */
	@RequestMapping("/annotation")
	public void annotation(){
		pointCutAnnotation.testResultMap("333333333333333333", "444444444444444444");
	}
	
	/**
	 * 解决aop同类切不到的问题
	 */
	@RequestMapping("/sameClass")
	public void sameClass(){
		pointCutSameClass.testParams2("K1", "V2");
	}
	
	
}

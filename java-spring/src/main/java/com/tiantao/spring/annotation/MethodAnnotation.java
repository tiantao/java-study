package com.tiantao.spring.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 关于方法的自定义注解
 * @author tiantao
 *
 */
@Target(ElementType.METHOD)			//标识该注解是方法的注解
@Retention(RetentionPolicy.RUNTIME)	//表示运行时的注解
public @interface MethodAnnotation {
	
	String value();
	
	/**
	 * 设置值，默认为""
	 * @return
	 */
	String desc() default "";
}

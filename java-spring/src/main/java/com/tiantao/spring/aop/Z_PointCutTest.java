package com.tiantao.spring.aop;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.tiantao.common.utils.CollectionsUtils;

/**
 * 创建被aop的类
 * @author tiantao
 */
@Component
public class Z_PointCutTest {
	
	/**
	 * 创建两个参数的方法
	 * @param a1
	 * @param a2
	 */
	public void testParams2(String a1,String a2){
		
	}
	
	/**
	 * 创建传入map参数的方法
	 * @param map
	 */
	public void testMap(Map<String,Object> map){
		
	}
	
	/**
	 * 创建传入2个参数，并返回map的方法
	 * @param a1
	 * @param a2
	 * @return
	 */
	public Map<String,Object> testResultMap(String a1,String a2){
		return CollectionsUtils.createHashMap("k1","V1","k2","V2");
	}
	
	
}

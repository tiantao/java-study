package com.tiantao.spring.aop;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * 如果同类调用同类的方法，那么aop是切不到的
 * 	如果实在想让aop切到，那么就需要调用该方法的时候，使用代理的方式调用，实现步骤如下
 * （注意springboot版本必须是高版本，最好是2.0.0以后的,否则，看文档：https://www.cnblogs.com/fanguangdexiaoyuer/p/7620534.html）
 * 1. 在springboot启动类中添加注解: @EnableAspectJAutoProxy(exposeProxy=true)
 * 2. 在同类调用同类方法的时候，先获取当前的代理类 proxyClazz = ((该类类名)AopContext.currentProxy())
 * 3. 通过代理类调用同类的方法，proxyClazz.testMap(..)
 * 
 * 注意aop的类必须是public，否则也是切不到的。
 *
 */
@Aspect					//作用是把当前类标识为一个切面供容器读取
@Component
public class SameClassAop {
	// 声明切面点
	@Pointcut("execution(public * com.tiantao.spring.aop.Z_PointCutSameClassTest.*(..))")
	public void pointcut(){}
	
	// 环绕增强，看注解与aop的结合
	@Around("pointcut()")
	public void doAround(ProceedingJoinPoint joinPoint){
		try {
			//1. 获取传入参数
			Object[] params = joinPoint.getArgs();
			String methodName = joinPoint.getSignature().getName();
			
			//2. 运行该方法
			System.out.println(methodName + " 方法的传入参数："+Arrays.toString(params));
			Object proceed = joinPoint.proceed(params);			//运行该方法
			System.out.println(methodName + " 方法返回值："+proceed);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}

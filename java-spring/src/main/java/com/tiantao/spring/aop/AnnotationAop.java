package com.tiantao.spring.aop;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

/**
 * 将自定义注解与aop关联使用
 * 	（用于实现给某些方法，增加特定的功能，可以变相实现装饰者模式）
  * 1. 需要添加jar
 *    <dependency>  
          <groupId>org.springframework.boot</groupId>  
          <artifactId>spring-boot-starter-aop</artifactId>  
      </dependency> 
   2. 声明切面点
   	   @Pointcut("@annotation(注解全类名)")
   
   3. 进行切面变成
	   @Before : 标识一个前置增强方法，相当于BeforeAdvice的功能
	   @AfterReturning : 后置增强，相当于AfterReturningAdvice，方法退出时执行
	   @AfterThrowing : 异常抛出增强，相当于ThrowsAdvice
	   @After : final增强，不管是抛出异常或者正常退出都会执行
	   @Around : 环绕增强，相当于MethodInterceptor
   4. JoinPoint对象的用法
   	   4.1 获取传入参数
   	   	Object[] params = joinPoint.getArgs();		//结果：[111111111, 22222222222222]
   	   4.2 获取当前被切方法的信息
   	    Signature signature = joinPoint.getSignature();		//里面存储该方法的类名，方法名，参数类型,method对象，class对象。。。
   	    signature.getDeclaringTypeName();				//该方法所在的类
   	    signature.getName();							//方法名
   	    Class<?> clazz = signature.getDeclaringType();	//获取当前类的class对象
   	    
   	    MethodSignature methodSignature = (MethodSignature) signature;
   	    Method method = methodSignature.getMethod();		//获取方法的Method对象
   	    Class[] types = methodSignature.getParameterTypes();//获取方法传入参数的类型
   	    methodSignature.getParameterNames();				//获取传入参数的名称
 *
 */
@Aspect					//作用是把当前类标识为一个切面供容器读取
@Component
public class AnnotationAop {
	
	// 声明切面点
	@Pointcut("@annotation(com.tiantao.spring.annotation.MethodAnnotation)")
	public void pointcut(){}
	

	// 前置增强
	@Before("pointcut()")
	public void doBefore(JoinPoint joinPoint){
		System.out.println("传入参数："+Arrays.toString(joinPoint.getArgs()));	//结果：[111111111, 22222222222222]
		Signature signature = joinPoint.getSignature();
		System.out.println("该方法所在类：" + signature.getDeclaringTypeName());	//结果：com.tiantao.spring.aop.Z_PointCutTest
		System.out.println("当前方法名：" + signature.getName());					//结果：testResultMap
		System.out.println("当前类class对象：" + signature.getDeclaringType());	//结果：class com.tiantao.spring.aop.Z_PointCutTest
		MethodSignature methodSignature = (MethodSignature) signature;
		System.out.println("当前方法的method对象："+methodSignature.getMethod());	//结果：public java.util.Map com.tiantao.spring.aop.Z_PointCutTest.testResultMap(java.lang.String,java.lang.String)
		System.out.println("当前方法的参数类型："+Arrays.toString(methodSignature.getParameterTypes()));//结果：[class java.lang.String, class java.lang.String]
		System.out.println("当前方法的参数名："+Arrays.toString(methodSignature.getParameterNames()));	 //结果：[a1, a2]
		System.out.println("当前方法的异常列表："+Arrays.toString(methodSignature.getExceptionTypes()));//结果：[]
		System.out.println("当前方法的返回值类型："+methodSignature.getReturnType());	//结果：interface java.util.Map
	}
	
	// 后置增强
	@AfterReturning(returning = "ret",pointcut = "pointcut()")
	public void doAfterReturning(JoinPoint joinPoint,Object ret){
		System.out.println("返回值："+ret);
	}
	
	//异常增强
	@AfterThrowing(throwing = "ex", pointcut = "pointcut()")
	public void throwing(JoinPoint joinPoint,Throwable ex){
		System.out.println("出现了异常："+ex.getMessage());
	}
	
	// 最终增强，不管是否抛出异常，都会执行
	@After("pointcut()")
	public void doafter(JoinPoint joinPoint){
		System.out.println("无论是否出现异常，都会执行！");
	}
	
	// 环绕增强，看注解与aop的结合
	@Around("pointcut()")
	public void doAround(ProceedingJoinPoint joinPoint){
		//1. 获取传入参数
		Object[] params = joinPoint.getArgs();
		
		//2. 运行该方法
		try {
			Object proceed = joinPoint.proceed(params);	//运行该方法
			System.out.println("Around该方法返回值："+proceed);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
}
